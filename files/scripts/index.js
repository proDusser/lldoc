"use strict"

import DataSet from '/scripts/DataSet.js'
import MorphIn from '/scripts/MorphIn.js';
import loadImg from '/scripts/loadImg.js';
import newEntry from '/scripts/addStuff.js';
import menuBar from '/scripts/menuBar.js';

export default function index() {
    let tiButton = {};document.getElementById('tB-1');
    let imgPic = document.getElementById('imgPic');
    let dataSet = new DataSet;
    imgPic.ondblclick = () => loadImg(imgPic);
    

    /** add stuff to window */
    window.firstLoad = true;
    window.firstFileLoad = true;
    window.arrMe = (domGroup, reArr = [], i = 0) => {
        for (i; i<domGroup.length; i++) reArr.push(domGroup[i]);
        return reArr
    };
    window.setSize = () => {
        loadImg(imgPic, 'setSize');
    };



    /** add event listeners to main Title */
    let persoListener = () => {
        let _mTitle = document.getElementById('mTitle');

        _mTitle.onclick = () => _mTitle.focus();
        _mTitle.focus = () => _mTitle.onfocus();
        _mTitle.onfocus = () => _mTitle.contentEditable = 'true';
        _mTitle.onfocusout = () => _mTitle.contentEditable = 'false';
    };




    menuBar(dataSet);
    tiButton = document.getElementById('tB-1');
    for (let i=0; i<8; i++) newEntry('pe', tiButton);
    persoListener();
    newEntry('ti', tiButton);
    loadImg(imgPic, 'edit');
    eListAppend();
    initSize();
    winResize();
    window.scroll(0, 0);
}
