"use strict"

export default class MorphIn {
    constructor (_id, _class) {
        this.id = _id;
        this.class = _class;
        this.div = document.createElement('div');
        this.div.id = _id;
        this.div.class = _class;
        this.div.className = _class;
        this.div.innerHTML = '<div></div>';
        this.div.style.minHeight = '1.65rem';
        this.div.style.width = 100 + '%';
        this.div.style.marginTop = '0.4rem';
        this.div.style.marginBottom = '0.4rem';
        this.div.style.paddingTop = '1px';
        this.div.style.paddingBottom = '1px';
        this.div.contentEditable = 'true';
        this.div.style.backgroundColor = 'whitesmoke';
        this.div.style.pointerEvents = 'auto';
        this.div.onblur = () => this.onblur();
        this.div.onclick = (event) => this.onclick(event);
        this.div.onfocus = () => this.onfocus();
        this.div.onkeyup = event => this.onkeyup(event);
        this.txMem = '----';
        this.nTxt = '';
        this.select = {};
        this.selOff = 0;
        this.overlay = document.getElementById('overlay');
        this.input1 = {};
        this.input2 = {};
        this.inCheck = false;
    }
    
    getSelection() {
        this.select = document.getSelection();
        this.selOff = this.select.focusOffset;
        this.nTxt = this.select.focusNode.textContent.substr(0, this.selOff);  
    }
    actMem(_key) {this.txMem = (this.txMem + _key).substring(this.txMem.length - 3) }
    newMem() {
        this.selOff >= 4
            ?this.txMem = this.nTxt.substr(this.selOff - 4, this.selOff)
            :this.txMem = '----'.substr(0, 4 - this.selOff) + this.nTxt;
    }
    eFilter(event) {
        [8, 13, 16, 17, 18, 35, 36, 46].indexOf(event.keyCode) !== -1
            ?this.newMem()
            :this.tFilter(event);
        this.div.innerHTML === '' ?this.div.innerHTML = '<div></div>' :'';
        this.div.style.height = 'fit-content'
    }
    tFilter(event, iHTML = this.div.innerHTML) {
        this.actMem(event.key);
        this.getSelection();
        this.txMem.toLowerCase() === 'www.' 
            ?this.goLink(event, this.select) 
            :iHTML.substr(0, 4) != '<div' 
                ?(this.div.contentEditable = 'false',
                this.getSelection(),
                this.div.innerHTML = '<div>' + iHTML.substr(0, iHTML.length -11) + '</div>',
                iHTML = this.div.innerHTML,
                this.select.setPosition(this.div.firstElementChild, 1),
                this.div.contentEditable = 'true') 
                :iHTML.length > 11 && iHTML.substr(iHTML.length - 11) === '<div></div>' 
                    ?(this.div.innerHTML = iHTML.substr(0, iHTML.length -11),
                    this.select.setPosition(this.select.focusNode, this.selOff))  
                    :'';
    }
    onfocus() {
        this.div.focus();
        this.div.contentEditable = 'true';
        typeof(window.morOnfocus) === 'function' ?window.morOnfocus(this.div) :'';
    }
    onclick(event) {
        this.onfocus();
        this.getSelection();
        this.newMem();
    }
    onblur() {
        this.div.contentEditable = 'false';
    }
    onkeyup(event) {
        this.getSelection();
        this.eFilter(event);
    }
    goLink(event, select, mylink = {}, iHTML = this.div.innerHTML) {
        let oLay = document.getElementById('overlay');
        let posi = iHTML.indexOf(this.select.focusNode.textContent) + this.select.focusOffset -4;
        let linkIn = this.linkReq(event, select);
        let selCont = this.select.focusNode;
        let selText = selCont.textContent;
        let selOffs = this.select.focusOffset;
        this.inCheck = false;

        oLay.style.height = document.body.clientHeight + 'px';
        oLay.style.width = document.body.clientWidth + 'px';
        oLay.style.top = 0;
        oLay.style.left = 0;
        oLay.style.display = 'block';
        oLay.parentNode.append(linkIn);
        let getStuff = () => {
            let linkUrl = document.getElementById('lIn_Url');
            let linkName = document.getElementById('lIn_Name');
            let noLBut = document.getElementById('lBut_NO');
            let addLBut = document.getElementById('lBut_ADD');
            linkIn = document.getElementById('linkInWin');
            this.input1 = linkUrl;
            this.input2 = linkName;
            this.input2.onfocus = () => this.inCheck = true;
            linkUrl.focus();
            noLBut.onclick = () => {
                mylink.check = false;
                this.div.innerHTML = iHTML.substr(0, iHTML.length - 2);
                this.onfocus();
                this.div.childNodes.forEach(x => x.textContent === selText 
                    ?selCont = x.childNodes[0] 
                    :x.childNodes.forEach(y => y.textContent === selText
                        ?selCont = y 
                        :''));
                document.getSelection().setPosition(selCont, selOffs);
                linkIn.remove();
                oLay.style.display = 'none';
            }
            addLBut.onclick = () => {
                mylink.check = true;
                mylink.url = linkUrl.value;
                mylink.name = linkName.value;
                linkIn.previousElementSibling.display = 'none';
                this.div.innerHTML = iHTML.substring(0, posi) + this.convertLink(mylink) + (iHTML.length > (posi + 8) ?iHTML.substring(posi + 4) :'');
                this.onfocus();
                this.div.childNodes.forEach(x => x.textContent.indexOf(selText.substr(0, selOffs -4) + mylink.name) != -1 
                    ?x.childNodes.forEach(y => y.textContent === mylink.name ?selCont = y.nextSibling :'') :'');
                document.getSelection().setPosition(selCont, 1);
                linkIn.remove();
                oLay.style.display = 'none';
            }
        }
        setTimeout(getStuff(), 200);
    }
    convertLink(linkData) {
        let _ind = linkData.url.indexOf('www.') === -1 ?0 :linkData.url.indexOf('www.') + 4;
        return '<a href="http://www.' + linkData.url.substring(_ind) + '">' + linkData.name + '</a>&nbsp'
    }
    makeEle(eType, eId, eClass, iHTML = '', newEle = {}) {
        eType === 'inPic' ?(newEle = document.createElement('input'), newEle.type = 'image')
          :eType === 'inUrl' ?(newEle = document.createElement('input'), newEle.type = 'url')
            :newEle = document.createElement(eType);
        newEle.id = eId;
        newEle.class = eClass;
        newEle.className = eClass;
        iHTML !== '' ?newEle.innerHTML = iHTML :'';
        eClass !== 'row' && eId.indexOf('box') === -1 ?newEle.style.pointerEvents = 'auto' :'';
        eClass === 'entries' ?newEle.onfocus = () => (focusBut(newEle)) :'';
        return newEle;
    }
    linkReq(event, select, lBut = {}, lIn = {}, lTit = {}) {
        let reLink = this.makeEle('div', 'linkInWin', 'editForm', '<div>Add a link ?</div>');
        let linkButs = this.makeEle('div', 'linkButs', 'btn-group');
        let pWith = this.select.focusNode.parentNode.parentNode.offsetWidth
        
        reLink.style.zIndex = '4';
        reLink.style.position = 'absolute';
        reLink.style.border = 'transparent'
        reLink.style.borderRadius = '0.25rem';
        reLink.style.backgroundColor = window.color.main;
        reLink.style.color = 'white';
        reLink.style.fontWeight = 'bold';
        reLink.style.margin = '0rem';
        reLink.style.padding = '0.5rem';
        reLink.style.left = event.target.getBoundingClientRect().left + "px";
        reLink.style.top = event.target.getBoundingClientRect().top + window.pageYOffset + "px";
        reLink.style.width = pWith + "px";
        reLink.style.height = '8.75rem';
        reLink.style.float = 'left';
        linkButs.style.marginLeft = '1rem';
        linkButs.style.width = (pWith * 0.75) + 'px';
        linkButs.style.float = 'left';
        linkButs.style.marginTop = '2px';
        linkButs.style.height = '2rem';
        linkButs.style.outline = 'none';
        reLink.firstElementChild.style.float = 'left';
        reLink.firstElementChild.style.width = '6.25rem';
        reLink.firstElementChild.style.height = '2rem';
        reLink.firstElementChild.style.paddingTop = '0.2rem';
        reLink.firstElementChild.style.textAlign = 'right';
        linkButs.role = 'group';
        ['NO' , 'ADD'].forEach(x =>
            (lBut = this.makeEle('button', 'lBut_' + x, 'btn btn-secondary', x === 'NO' ?'NO' : 'ADD'),
            lBut.style.border = 'transparent',
            lBut.style.width = '49.75%',
            x === 'ADD' ?lBut.style.marginLeft = '0.5%' :'',
            lBut.style.height = '1.8rem',
            lBut.style.padding = '0px',
            linkButs.append(lBut)));
        reLink.append(linkButs);
        
        ['Url', 'Name'].forEach(y =>
            (lIn = this.makeEle('input', 'lIn_' + y, 'linkIns'),
            y === 'Url' ?(lIn.value = 'www.', lIn.onkeyup = () => this.lInkeyup(event)) :'',
            lTit = this.makeEle('div', 'lTit_' + (y === 'Url' ?'url' :'name'), 'lTit', y === 'Url' ?'link-adress:' :'link-name:'),
            lIn.style.height = '1.8rem',
            lTit.style.height = '1.8rem',
            lIn.style.width = (pWith * 0.75) + 'px',
            lTit.style.width = '6.25rem',
            lIn.style.marginLeft = '1rem',
            lIn.style.marginTop = '1rem',
            lTit.style.marginTop = '1rem',
            lTit.style.paddingTop = '0.15rem',
            lIn.style.borderRadius = '0.25rem',
            lTit.style.float = 'left',
            lTit.style.textAlign = 'right',
            reLink.append(lTit),
            reLink.append(lIn)));

        return reLink;
    }
    lInkeyup(event) {
        !this.inCheck 
            ?this.input1.value.toLowerCase().indexOf('www.') === -1
                ?this.input2.value = this.input1.value
                :this.input2.value = this.input1.value.substr(this.input1.value.toLowerCase().indexOf('www.') + 4)
            :'';
    }
    supMethod(method) {};
    margin(value) {this.div.style.margin = value + '%'}
    padding(value) {this.div.style.padding = value + '%'}
    textMargin(value) {this.div.childNodes.forEach(x => x.style.margin = value + '%') }
    textPadding(value) {this.div.childNodes.forEach(x => x.style.padding = value + '%') }
}
