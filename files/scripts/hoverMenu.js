"use strict"

import domForge from '/scripts/domForge.js';

/** returns hover menues for textsize and line spacing
 * hType ('perso', 'title')
 * hFrameParent (frame to add hFrame)
 */
export default function hoverMenu (hType, tiNr) {
    let rows = {};
    let titles = {};
    let persos = {};
    
    /* title & perso elements */
    let hFrame = domForge('div', 'frameH', 'hMenu','','', hType === 'perso' ? 'pe' :'ti');
    let hTitle0 = domForge('div', 'title0H', 'hMenu', '', 'Spacing');
    let hTitle1 = domForge('div', 'title1H', 'hMenu', '', 'Title-size');
    let hTitle2 = domForge('div', 'title2H', 'hMenu', '', 'Text-size');
    let hStretch = domForge('input', 'imgBHover0', 'hMenu', './images/icons/stretch_blue.svg');
    let hPress = domForge('input', 'imgBHover1', 'hMenu', './images/icons/press_blue.svg');
    let hTSize0 = domForge('input', 'tSizeH0_' + tiNr, 'hMenu');
    let hTSize1 = domForge('input', 'tSizeH1_' + tiNr, 'hMenu');
    let hArrDiv0 = domForge('div', 'divHArrow0', 'hMenu');
    let hArrDiv1 = domForge('div', 'divHArrow1', 'hMenu');
    let hUpArrow0 = domForge('input', 'arrowHUp0', 'hMenu', './images/icons/arrow_u_blue.svg');
    let hUpArrow1 = domForge('input', 'arrowHUp1', 'hMenu', './images/icons/arrow_u_blue.svg');
    let hDownArrow0 = domForge('input', 'arrowHDown0', 'hMenu', './images/icons/arrow_d_blue.svg');
    let hDownArrow1 = domForge('input', 'arrowHDown1', 'hMenu', './images/icons/arrow_d_blue.svg');
    /* spacer elements */
    let docSpacer = domForge('div', 'docSpacer_' + tiNr, 'docSpacer');
    let spaceBut0 = domForge('input', 'spaceBut0', 'docSpacer', './images/icons/moveOut_blue.svg')
    let spaceBut1 = domForge('input', 'spaceBut1', 'docSpacer', './images/icons/moveIn_blue.svg')


    /* store the y position of the element to prevent scrolling/reposition */
    let rectY = 0;
    let rects = event => rectY = event.target.getBoundingClientRect().y;
    let rePosi = event => {
        scrollTo(0, (event.target.getBoundingClientRect().y - document.body.getBoundingClientRect().y - rectY));
    }


    /* add event listeners */
    hType === 'perso'
        ?(hTSize0.value = Number(window.textSize.perso),
        hFrame.onmouseover = () => generatePersos(),
        hStretch.onclick = (event) => (rects(event), doHEditPerso(1, event)),
        hPress.onclick = (event) => (rects(event), doHEditPerso(-1, event)),
        hTSize0.onkeyup = (event) => keyCheck(event, 'pe'),
        hTSize0.onchange = (event) => eHandler(event, 'pe'),
        hTSize0.onpaste = (event) => pasteCheck(event, 'pe'),
        hUpArrow0.onclick = (event) => (rects(event), doHSize(1, 0, 0, event)),
        hDownArrow0.onclick = (event) => (rects(event), doHSize(-1, 0, 0, event)),
        
        hFrame.append(hTitle1),
        hFrame.append(hTitle0),
        hArrDiv0.append(hUpArrow0),
        hArrDiv0.append(hDownArrow0),
        hFrame.append(hArrDiv0),
        hFrame.append(hTSize0),
        hFrame.append(hPress),
        hFrame.append(hStretch))
        :'';


    hType === 'title'
        ?(hTSize0.value = Number(window.textSize.title),
        hTSize1.value = Number(window.textSize.entries),
        hFrame.onmouseover = () => (generateTitles(), generateRows()),
        hStretch.onclick = (event) => (rects(event), doHEditText(1, event)),
        hPress.onclick = (event) => (rects(event), doHEditText(-1, event)),
        hTSize0.onkeyup = (event) => keyCheck(event, 'ti'),
        hTSize1.onkeyup = (event) => keyCheck(event, 'tx'),
        hTSize0.onchange = (event) => eHandler(event, 'ti'),
        hTSize1.onchange = (event) => eHandler(event, 'tx'),
        hTSize0.onpaste = (event) => pasteCheck(event, 'ti'),
        hTSize1.onpaste = (event) => pasteCheck(event, 'tx'),
        hUpArrow0.onclick = (event) => (rects(event), doHSize(0, 1, 0, event)),
        hUpArrow1.onclick = (event) => (rects(event), doHSize(0, 0, 1, event)),
        hDownArrow0.onclick = (event) => (rects(event), doHSize(0, -1, 0, event)),
        hDownArrow1.onclick = (event) => (rects(event), doHSize(0, 0, -1, event)),
        
        hFrame.append(hTitle2),
        hFrame.append(hTitle1),
        hFrame.append(hTitle0),
        hArrDiv1.append(hUpArrow1),
        hArrDiv1.append(hDownArrow1),
        hFrame.append(hArrDiv1),
        hFrame.append(hTSize1),
        hArrDiv0.append(hUpArrow0),
        hArrDiv0.append(hDownArrow0),
        hFrame.append(hArrDiv0),
        hFrame.append(hTSize0),
        hFrame.append(hPress),
        hFrame.append(hStretch))
        :'';


    hType === 'spacer'
        ?(docSpacer.onmouseover = () => (
            docSpacer.style.borderTopStyle = 'dashed', 
            docSpacer.style.borderBottomStyle = 'dashed',
            docSpacer.style.borderColor = window.color.main.substr(0, window.color.main.length -1) + ', 0.7)',
            spaceBut0.style.display = 'block',
            spaceBut1.style.display = 'block',
            docSpacer.style.backgroundColor = window.color.shade),
        docSpacer.onmouseleave = () => (
            docSpacer.style.borderTopStyle = 'none', 
            docSpacer.style.borderBottomStyle = 'none',
            spaceBut0.style.display = 'none',
            spaceBut1.style.display = 'none',
            docSpacer.style.backgroundColor = window.color.backGround),
        spaceBut0.onmouseover = () => (
            docSpacer.style.borderTopStyle = 'dashed', 
            docSpacer.style.borderBottomStyle = 'dashed',
            docSpacer.style.borderColor = window.color.main.substr(0, window.color.main.length -1) + ', 0.7)',
            spaceBut0.style.display = 'block',
            spaceBut1.style.display = 'block',
            docSpacer.style.backgroundColor = window.color.shade),
        spaceBut1.onmouseover = () => (
            docSpacer.style.borderTopStyle = 'dashed', 
            docSpacer.style.borderBottomStyle = 'dashed',
            docSpacer.style.borderColor = window.color.main.substr(0, window.color.main.length -1) + ', 0.7)',
            spaceBut0.style.display = 'block',
            spaceBut1.style.display = 'block',
            docSpacer.style.backgroundColor = window.color.shade),
        
        spaceBut0.onclick = event => spaceOut(event),
        spaceBut1.onclick = event => spaceIn(event),

        docSpacer.append(spaceBut0),
        docSpacer.append(spaceBut1),
        docSpacer.style.height = '64px')
        :'';
    








    let keyCheck = (event, hKey) => {
        isNaN(event.key) ?event.target.value = Number(event.target.value.toString().substr(0, event.target.value.toString().lengt -1)) :'';
        eHandler(event, hKey);
    }
    let pasteCheck = (event, hKey) => isNaN(event.clipboardData.getData('text')) ?event.preventDefault() :eHandler(event, hKey);
    let eHandler = (event, hKey) => {
        rects(event);
        hKey === 'ti'
            ?(window.textSize.title = Number(hTSize0.value),
            doTitleResize(event))
            :'';
        hKey === 'tx'
            ?(window.textSize.entries = Number(hTSize1.value),
            doTextResize(event))
            :'';
        hKey === 'pe'
            ?(window.textSize.perso = Number(hTSize0.value),
            doPersoResize(event))
            :'';
    }


    

    /** update element collection on mouseenter of parents */
    let generatePersos = () => {
        persos = arrMe(document.getElementsByClassName('persIn'));
    }
    let generateTitles = () => {
        titles = arrMe(document.getElementsByClassName('title'));
    }    
    let generateRows = () => {
        rows = arrMe(document.getElementsByClassName('entries'));
    }

    /** execute edits */
    /* +- perso margins */
    let doHEditPerso = (peV, event) => {            // (peV = -1/0/1) press/pass/stretch
        window.textSize.marginPerso += peV/10;
        window.textSize.marginPerso < 0.1 ?window.textSize.marginPerso = 0.1 :'';
        persos.forEach(x => {
            x.style.marginTop = Math.round(window.textSize.marginPerso * window.rFSize) + 'px';
            x.style.marginBottom = Math.round(window.textSize.marginPerso * window.rFSize) + 'px';
        });
        peV < 0 ?fiPicLeveler() :'';
        rePosi(event);
    }   
    /* +- text&title margins */
    let doHEditText = (txV, event) => {            // (txV = -1/0/1) press/pass/stretch
        window.textSize.marginText += txV/10;
        window.textSize.marginText < 0.1 ?window.textSize.marginText = 0.1 :'';
        rows.forEach(x => {
            x.style.marginTop = Math.round(window.textSize.marginText * window.rFSize) + 'px';
            x.style.marginBottom = Math.round(window.textSize.marginText * window.rFSize) + 'px';
        });
        titles.forEach(y => {
            y.style.marginBottom = Math.round(window.textSize.marginText * window.rFSize) + 'px';
        })

        rePosi(event);
    }

    let doHSize = (peS, tiS, txS, event) => {            // ((peS,tiS,txS) = -1/0/1) smaller/pass/bigger
        window.textSize.perso = Number(window.textSize.perso) + Number(peS);
        window.textSize.title = Number(window.textSize.title) + Number(tiS);
        window.textSize.entries = Number(window.textSize.entries) + Number(txS);
        [window.textSize.perso, window.textSize.title, window.textSize.entries].forEach(x => x < 1 ?x = 1 :'');

        hType === 'title' 
            ?(hTSize0.value = window.textSize.title, 
            hTSize1.value = window.textSize.entries,
            doTitleResize(event),
            doTextResize(event))
            :'';
        hType === 'perso' 
            ?(hTSize0.value = window.textSize.perso,
            doPersoResize(event))
            :'';
        peS < 0 ?fiPicLeveler() :'';
        
        rePosi(event);
    }

    let doPersoResize = (event) => {
        persos.forEach(x => x.style.fontSize = (window.textSize.perso/12) + 'rem');
        rePosi(event);
    }
    let doTitleResize = (event) => {
        titles.forEach(x => x.style.fontSize = (window.textSize.title/12) + 'rem');
        rePosi(event);
    }
    let doTextResize = (event) => {
        rows.forEach(x => {
            x.id.indexOf('va2') !== -1 
                ?(x.style.fontSize = (window.textSize.entries/12) + 'rem',
                x.style.minHeight = x.parentNode.parentNode.firstElementChild.firstElementChild.clientHeight + 'px' )
                :x.style.fontSize = (window.textSize.entries/12) + 'rem';
        });
        rePosi(event);
    }

    /* spacer edits */
    let spaceOut = event => {
        event.target.parentNode.clientHeight > (300 * window.textSize.rFSize)
            ?event.target.parentNode.style.height = (300 * window.textSize.rFSize) + 'px'
            :event.target.parentNode.style.height = (event.target.parentNode.clientHeight + window.textSize.rFSize) + 'px';
        scrollBy(0,  (window.textSize.rFSize / 2));
    }    
    let spaceIn = event => {
        event.target.parentNode.clientHeight <  (2.5 *window.textSize.rFSize)
            ?event.target.parentNode.style.height = (1.5 * window.textSize.rFSize) + 'px'
            :event.target.parentNode.style.height = (event.target.parentNode.clientHeight - window.textSize.rFSize) + 'px';
            scrollBy(0, -(window.textSize.rFSize / 2));
    }


    if (hType === 'spacer') {
        return docSpacer;
    } else {
        return hFrame;
    }
}
