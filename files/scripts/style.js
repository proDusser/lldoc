"use strict"
/**  resizing and styling */

let persInfos = () => document.getElementsByClassName('persIn');
let rootCont = document.getElementById('rootCont');
let menuCont = document.getElementById('menuCont');
let mainCont = document.getElementById('mainCont');
let menuField = document.getElementById('fiInfo');
let persoField = document.getElementById('pers0');
let imageField = document.getElementById('fiPic');
let mainText = document.getElementsByClassName('entries');
let titleText = document.getElementsByClassName('title');
let overlay = document.getElementById('overlay');
let _imgPic = document.getElementById('imgPic');
let root = document.querySelector(":root");
let rFSize = Number(root.style.fontSize.substr(0, root.style.fontSize.indexOf('px')));

let persoButField = () => document.getElementById('persoBox');

/** add arguments to window */
window.color = {};
window.color.main = 'rgb(148, 182, 224)';
window.color.shine = 'rgb(245, 122, 77)';
window.color.shade = 'rgb(245, 245, 253)';
window.color.input = 'rgb(255, 255, 255)';
window.color.backGround = 'rgb(255, 255, 255)';
window.color.text = 'rgb(255, 255, 255)';
window.textSize = {};
window.textSize.perso = 12;
window.textSize.title = 14;
window.textSize.entries = 12;
window.rFSize = 16; 
window.textSize.rFSize = 16; 
window.textSize.marginText = 0.4;
window.textSize.marginPerso = 0.4;

window.onresize = () => winResize();

let persIns = arrMe(persInfos());


let titleReSize = (val) => {
    let tits = arrMe(document.getElementsByClassName('title'));
    tits.forEach(x => x.style.fontSize = (val/12) + 'rem');
}
let rowReSize = (val) => {
    let rows = arrMe(document.getElementsByClassName('entries'));
    rows.forEach(x => x.style.fontSize = (val/12) + 'rem');
}
let persoReSize = (val) => {
    let persos = arrMe(document.getElementsByClassName('persIn'));
    persos.forEach(x => {
        x.style.fontSize = (val/12) + 'rem';
        x.style.marginTop = (window.textSize.marginPerso * window.textSize.rFSize) + 'px';
        x.style.marginBottom = (window.textSize.marginPerso * window.textSize.rFSize) + 'px';
    });
}
let initSize = () => {
    menuCont.style.height = window.innerHeight + 'px';
    rootCont.style.minHeight = window.innerHeight + 'px'
    titleReSize(window.textSize.title);
    rowReSize(window.textSize.entries);
    persoReSize(window.textSize.perso);
}
let sizeEntries = tiDi => {
    tiDi.lastElementChild.firstElementChild.style.minHeight = tiDi.firstElementChild.firstElementChild.clientHeight + 'px';
    tiDi.childNodes.forEach(x => {
        x.firstElementChild.style.marginTop = (window.textSize.marginText * window.textSize.rFSize) + 'px';
        x.firstElementChild.style.marginBottom = (window.textSize.marginText * window.textSize.rFSize) + 'px';
    })
}


/** on window resize */

let winResize = (oFlow = () => (window.innerWidth - 1130) / 2) => {
    oFlow() < 0 ?oFlow = () => (window.innerWidth - 1000) / 2 :'';
    window.innerWidth < 1130 
        ?menuCont.slide ?menuSlideOut() :''
        :menuCont.slide ?'' :menuSlideIn();
    oFlow() < 0 ?oFlow = () => 0 :'';
    mainCont.style.marginLeft = (oFlow() + (menuCont.slide ?145 :20)) + 'px';
    mainCont.style.marginRight = oFlow() + 'px';
    menuCont.style.height = window.innerHeight + 'px';
    rootCont.style.minHeight = window.innerHeight + 'px'
    overlay.style.height = window.innerHeight + 'px';
    overlay.style.width = window.innerWidth + 'px';
}
let menuSlideOut = () => {
    menuCont.slide = false;
    menuCont.style.marginLeft = '-120px';
    menuField.firstElementChild.style.display = 'none';
    menuField.firstElementChild.nextElementSibling.style.marginLeft = '8.125rem';
    menuField.firstElementChild.nextElementSibling.innerHTML = '>>';
}
let menuSlideIn = () => {
    menuCont.slide = true;
    menuCont.style.marginLeft = '0px';
    menuField.firstElementChild.style.display = 'inline-block';
    menuField.firstElementChild.nextElementSibling.style.marginLeft = '0rem';
    menuField.firstElementChild.nextElementSibling.innerHTML = '<<';
}
/** fiPicLeveler sets the size of the image-frame according to the size of the Perso entries
 * window.setSize() resizes and resets the image itself by calling laodImg(img, 'setSize')
 */
let fiPicLeveler = () => {
    persoButField().style.display === 'none'
        ?(imageField.style.height = persoField.clientHeight + 'px',
        imageField.style.marginBottom = window.textSize.marginPerso + 'rem')
        :(imageField.style.height = (persoField.clientHeight - persoButField().clientHeight) + 'px',
        imageField.style.marginBottom = window.textSize.marginPerso + 'rem')
    window.setSize();
}

let eListAppend = () => {
    let fiPers = document.getElementById('fiPers');
    fiPers.onmouseenter = () => (fiPers.style.backgroundColor = window.color.shade, persIns.forEach(y => y.style.backgroundColor = window.color.input));
    fiPers.onmouseleave = () => (fiPers.style.backgroundColor = window.color.input, persIns.forEach(z => z.style.backgroundColor = window.color.shade));
    fiPers.childNodes.forEach(x => x.onmouseenter = () => (fiPers.style.backgroundColor = window.color.shade));


    let fiMTitle = document.getElementById('fiMainTitle');
    fiMTitle.onmouseenter = () => {
        fiMTitle.style.backgroundColor = window.color.shade;
        fiMTitle.firstElementChild.style.backgroundColor = window.color.input;
        fiMTitle.style.paddingLeft = '2.5rem';
        fiMTitle.style.paddingRight = '2.5rem';
    };
    fiMTitle.onmouseleave = () => {
        fiMTitle.style.backgroundColor = window.color.backGround;
        fiMTitle.firstElementChild.style.backgroundColor = window.color.shade;
        fiMTitle.style.paddingLeft = '2.5rem';
        fiMTitle.style.paddingRight = '2.5rem';
    }
    fiMTitle.firstChild.onmouseenter = () => (fiMTitle.style.backgroundColor = window.color.shade);
}

let setColM = cEle => {
    let ents = arrMe(cEle.getElementsByClassName('entries')).concat(arrMe(cEle.getElementsByClassName('title')));
    ents.forEach(x => x.style.backgroundColor = window.color.input); 
}
let setColS = cEle => {
    let ents = arrMe(cEle.getElementsByClassName('entries')).concat(arrMe(cEle.getElementsByClassName('title')));
    ents.forEach(x => x.style.backgroundColor = window.color.shade); 
}




menuCont.slide = true;
rootCont.style.backgroundColor = window.color.shade.substr(0, window.color.main.length -1) + ', 0.2)';
menuCont.style.backgroundColor = window.color.main;
mainCont.style.backgroundColor = window.color.backGround;
mainCont.style.borderColor = window.color.shade;