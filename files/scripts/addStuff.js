"use strict"

/** addStuff adds new Titles and Rows to the document and
 *  helps organize them.
 * 
 * newEntry()
 *      eType - 'row' or 'title'
 *      eDom - Dom Element that fired newEntry()
 */

import domForge from '/scripts/domForge.js';
import MorphIn from '/scripts/MorphIn.js';
import hoverMenu from './hoverMenu.js';

export default function newEntry (eType, eDom, eNameArr = eDom.id.split("_")) {
    
    let myDoc = document.getElementById('fiList');
    let myPerso = document.getElementById('pers0');
    let sort = {isTo: false, isEnd: true, eDom: {}};

    /** Document Element creation
     * 
     * newTitle() - adds a new Title with it's first row
     * newRow() - adds a new row
     * focusBut() - lets the fButtons spawn [up/down/delete]
     */


    let newPerso = (pNr = myPerso.childElementCount) => {
        let makeButs = () => {
            let persoButBox = domForge('div', 'persoBox', 'persoButs');
            let addPersoBut = domForge('button', 'addPersoBut', 'persoButs', '', 'add line');
            let delPersoBut = domForge('button', 'delPersoBut', 'persoButs', '', 'delete line');
            
            myPerso.myHover = hoverMenu('perso', 99999);
            myPerso.onDelete = false;

            myPerso.onmouseover = () => {
                myPerso.onDelete = false;
                myPerso.style.backgroundColor = window.color.shade;
                myPerso.childNodes.forEach(x => x.className === 'persIn' ?x.style.backgroundColor = window.color.backGround :'');
                myPerso.myHover.style.display = 'inline-block';
                document.getElementById('tSizeH0_99999').value = window.textSize.perso;
                persoButBox.style.display = 'block';
            }            
            myPerso.onmouseleave = () => {
                myPerso.onDelete
                    ?''
                    :(myPerso.style.backgroundColor = window.color.backGround,
                    myPerso.childNodes.forEach(x => x.className === 'persIn' ?x.style.backgroundColor = window.color.shade :''),
                    myPerso.myHover.style.display = 'none',
                    persoButBox.style.display  = 'none',
                    fiPicLeveler()
                    )
            }
            addPersoBut.onclick = () => {
                newEntry('pe', addPersoBut);
                persoReSize();
                fiPicLeveler();
            }
            delPersoBut.onclick = () => {
                let persos = window.arrMe(document.getElementsByClassName('persIn'));
                myPerso.onDelete = true;
                persos.length > 1 
                    ?myPerso.lastElementChild.previousElementSibling.remove() 
                    :'';
                fiPicLeveler();
                myPerso.style.display = 'block';
        }
            
            
            persoButBox.append(delPersoBut);
            persoButBox.append(addPersoBut);
            myPerso.append(myPerso.myHover);
            myPerso.append(persoButBox);
        }

        pNr === 0 ?makeButs() :pNr += -2;

        let newPersIn = domForge('input', 'pers0_va' + pNr, 'persIn');
        newPersIn.onmouseover = () => newPersIn.style.backgroundColor = window.color.input;
        myPerso.insertBefore(newPersIn, myPerso.lastElementChild);
    }

    /* add a new Title with its first row and a spacer*/
    let newTitle = () => {
        let tiNr = Number(eNameArr[0].slice(2)) + 1
        let row1 = domForge('div', 'box_ti' + tiNr, 'row', 'exc_newTitle');
        let row2 = domForge('div', 'box_rB' + tiNr, 'row', 'exc_newTitle');
        let newTiBox = domForge('div', 'ti' + tiNr, 'row', 'exc_newTitle');
        let tiTxt = domForge('input', 'ti' + tiNr + '_va0', 'title', 'exc_newTilte');
        let roBut = domForge('button', 'rB' + tiNr + '_ro-1', 'roButs', 'exc_newTitle', 'new line');

        
        roBut.onclick = () => newEntry('row', event.target);
        
        row1.onmouseover = () => {
            row1.style.backgroundColor = window.color.shade;
            setColM(row1);
            row1.myHover.style.display = 'inline-block';
            row2.style.display = 'block';
            document.getElementById('tSizeH0_' + tiNr).value = window.textSize.title;
            document.getElementById('tSizeH1_' + tiNr).value = window.textSize.entries;
        };
        row1.onmouseleave = () => {
            row1.style.backgroundColor = window.color.backGround;
            setColS(row1);
            row1.myHover.style.display = 'none';
            row2.style.display = 'none';
        };
        row2.onmouseover = () => row2.style.display = 'block';
        
        
        row1.myHover = hoverMenu('title', tiNr);
        row1.append(row1.myHover);        
        newTiBox.append(tiTxt);
        row1.append(newTiBox);
        row2.append(roBut);
        row1.append(row2);

        myDoc.mySpacer = hoverMenu('spacer', tiNr);
        myDoc.append(myDoc.mySpacer);
        myDoc.append(row1);

        /*manipulate eDom for newRow function*/
        eDom.id = 'tB' + (Number(eDom.id.substr(2)) + 1);
        eDom = roBut;
        eNameArr = eDom.id.split("_");

        /* add first row */
        newRow(0);
    }


    /* add a new row */
    let newRow = (count, newIn = '', newBox = '', tiPartId = Number(eDom.id.split('_')[0].substr(2))) => {
        let _parentNode = document.getElementById('box_ti' + tiPartId);
        let rowX = domForge('div', 'box_ti' + tiPartId + '_ro' + count, 'row', 'exc_newRow');
        let newRo = domForge('div', 'ti' + tiPartId + '_ro' + count, 'tiDiv', 'exc_newRow');
        
        count !== 0 ?eDom.myY = eDom.getBoundingClientRect().y :'';

        for (let i = 0; i <= 2; i++) {
            let _id = 'ti' + tiPartId + '_ro' + count + '_va' + i;
            i !== 2
                ?(newBox = domForge('div', 'box_' + _id, 'col-md-2', 'exc_newRow'),
                newIn = domForge('input', _id, 'entries', 'exc_newRow', 'va' + i))
                :(newBox = domForge('div', 'box_' + _id, 'col-md-8', 'exc_newRow'), 
                newBox.myMorphIn = new MorphIn(_id, 'entries'), 
                newIn = newBox.myMorphIn.div, 
                newIn.style.backgroundColor = window.color.shade
            );

            newIn.onfocus = () => (focusBut(newIn));
            newBox.append(newIn);
            newRo.append(newBox);
        }
        /* append to document */
        rowX.append(newRo);
        _parentNode.insertBefore(rowX, _parentNode.lastElementChild);
        /* move 'add Row' button to bottom */
        tiMove();
        /* size entries */
        sizeEntries(newRo);
        /* scroll to new row */
        window.firstLoad 
            ?window.firstLoad = false 
            :count === 0 
                ?window.scrollTo(0, window.scrollMaxY)
                :window.scrollTo(0, eDom.getBoundingClientRect().y - document.body.getBoundingClientRect().y - eDom.myY);

    }


    /* adds the edit buttons on focus */
    let focusBut = dObj => {
        if (sort.isEnd) {
            let fButs = document.getElementsByClassName('ediButs');
            while (dObj.id === '' || dObj.id === undefined) dObj = dObj.parentNode;
            if (fButs.length > 0 && fButs[0].id.substr(0, fButs[0].id.indexOf('_va') === dObj.id.substr(0, dObj.id.indexOf('_va')))) {
                '';
            } else {
                fButRemove();
                let dArr = dObj.id.split('_');
                let pObj = document.getElementById('box_' + dArr[0]);
                let butBox = domForge('div', dObj.id.substr(0, dObj.id.indexOf('va')) + 'buts', 'row', 'exc_focusButs');

                for (let i = 0; i <= 2; i++) {
                    let nBut = domForge('button', dObj.id + '_eB' + i, 'ediButs', 'exc_focusButs', i === 2 ?'Down' : i === 1 ?'Up' :'Delete');
                    nBut.onclick = event => pressMe(event.target);
                    butBox.append(nBut);
                }
                pObj.insertBefore(butBox, dObj.parentNode.parentNode.parentNode.nextElementSibling);
            }
        } else {
            let goFButs = () => (focusBut(dObj), clearInterval(waitSort));
            let waitSort = setInterval(goFButs(), 150);
        }
        dObj.focus();
    }





    /**
     * NODE EDIT (fButtons[up,down,delete], sortFunctions, id-reNaming)
     * pressMe() - evaluates case
     * killMe() - deletes Row
     * upMe() - Moves Row up
     * downMe() - Moves Row
     * scrollToBottom() - Brings object to the bottom of the window
     * scrollToTop() - Brings object to the Top of the window
     * sortMe() - sorts Title
     * rowRename() - renames ids of a row
     * fButsrename() - rename ids of fButtons
     * renameID() - renames the id of an oject
     * tiMove() - Moves rButton to the bottom of the Title
     * fButRemove() - Removes the fButtons
     * shapeBut() - shapes Buttons
     **/

    let pressMe = (dObj, dArr = dObj.id.split('_')) => {
        dObj.parentNode.myY = dObj.parentNode.getBoundingClientRect().y;
        dArr[3] === 'eB2' ?downMe(dObj)
            :dArr[3] === 'eB1' ?upMe(dObj)
                :dArr[3] === 'eB0' ?killMe(dObj) :'';
    }
    let killMe = (dObj, dPar = dObj.parentNode, dSort = dPar.parentNode) => {
        dPar.previousElementSibling.remove();
        dPar.remove();
        sort.eDom = dSort;
        sort.isEnd 
            ?(sort.isEnd = false, sort.isTo = false, sortMe()) 
            :(sort.isTo = true);
    }
    let upMe = (dObj, dPar = dObj.parentNode) => {
        let moveObj = dPar.previousElementSibling;
        dPar.previousElementSibling.previousElementSibling.id.length > 3 
            ?(dPar.parentNode.insertBefore(moveObj, moveObj.previousElementSibling), dPar.parentNode.insertBefore(dPar, dPar.previousElementSibling)) 
            :'';
        sort.eDom = dPar.parentNode; 
        sort.isEnd 
            ?(sort.isEnd = false, sort.isTo = false, sortMe()) 
            :(sort.isTo = true);
        }
    let downMe = (dObj, dPar = dObj.parentNode) => {
        let moveObj = dPar.previousElementSibling;
        dPar.nextElementSibling.id.indexOf('box_rB') < 0
            ?(dPar.parentNode.insertBefore(dPar, dPar.nextElementSibling.nextElementSibling), dPar.parentNode.insertBefore(moveObj, dPar)) 
            :'';
        sort.eDom = dPar.parentNode;  
        sort.isEnd 
            ?(sort.isEnd = false, sort.isTo = false, sortMe()) 
            :(sort.isTo = true);
    }
    let scrollToPosi = (sObj) => {
        window.scrollTo(0, sObj.getBoundingClientRect().y - document.body.getBoundingClientRect().y - sObj.myY)
    }


    let sortMe = (count = 0) => {
        let endSet = () => sort.isEnd = true;
        let delayedSort = () => {
                let _eDom = document.getElementById(sort.eDom.id);
                let eColl = _eDom.childNodes;
                eColl.forEach(x => x.id.indexOf('box_ti') >= 0
                    ?(rowRename(x, count), count++)
                    :x.id.indexOf('buts') >= 0
                        ?fButsRename(x, count)
                        :x.id.indexOf('rB') >= 0
                            ?(renameID(x.firstElementChild, x.firstElementChild.id.indexOf('_ro') + 3, count - 1),
                            sort.isTo 
                                ?(sort.isTo = false,
                                sortMe())
                                :setTimeout(endSet, 50))
                            :''
                );
        }
        setTimeout(delayedSort, 50);
    }
    let rowRename = (_rBox, rCount, rColl = _rBox.firstElementChild.childNodes) => {
        rColl.forEach(y => (
            renameID(y.firstElementChild, y.firstElementChild.id.indexOf('_ro') + 3, rCount),
            renameID(y, y.id.indexOf('_ro') + 3, rCount)
        ));
        renameID(_rBox.firstElementChild, _rBox.firstElementChild.id.indexOf('_ro') + 3, rCount);
        renameID(_rBox, _rBox.id.indexOf('_ro') + 3, rCount);   
    }
    let fButsRename = (_fButs, fCount, fBColl = _fButs.childNodes) => {
        fBColl.forEach(y => renameID(y, y.id.indexOf('_ro') + 3, fCount - 1));
        renameID(_fButs, _fButs.id.indexOf('_ro') + 3, fCount - 1);
        scrollToPosi(_fButs)
    }
    let renameID = (rObj, rIndex, rValue) => {
        let restID = rObj.id.substr(rIndex).split('').findIndex(x => isNaN(x));
        rObj.id = rObj.id.substr(0, rIndex) + rValue + (restID !== -1 ?rObj.id.substr(restID + rIndex) :'');
    }
    let tiMove = () => {
        let nexBut = eDom;
        let parent = eDom.parentNode.parentNode;
        nexBut.id = eNameArr[0] + '_' + eNameArr[1].slice(0, 2) + (Number(eNameArr[1].slice(2))+1);
        nexBut = nexBut.parentNode;
        eDom.parentNode.remove();
        parent.append(nexBut);
    }
    let fButRemove = () => {
        let fButs = document.getElementsByClassName('ediButs');
        fButs.length > 0?fButs[0].parentNode.remove() :'';
    }



    /** window.morOnfocus()
     *  adds event-listeners to the window which can be used by the MorphIn class
     */
    window.morOnfocus = _dObj => focusBut(_dObj);

    
    /** evaluate newEntry(eType) case */

    eType === 'ti' 
        ?newTitle()
        :eType[0] === 'r' 
            ?newRow(Number(eNameArr[1].slice(2)) + 1)
            :eType === 'pe'
                ?newPerso()
                :'';
}