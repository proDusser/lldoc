"use strict"

import DataSet from '/scripts/DataSet.js';
import newEntry from '/scripts/addStuff.js';
import domForge from '/scripts/domForge.js';

export default function menuBar(dataSet) {
    let mCont = document.getElementById('menuCont');
    let mField = document.getElementById('fiInfo');

    let mTi_file = domForge('div', 'mTi_file', 'menuBar', '', 'File');
    let mTi_document = domForge('div', 'mTi_document', 'menuBar', '', 'Document');
    let mBu_menu = domForge('button', 'mBu_menu', 'menuBar', '', '<<');
    let mBu_save = domForge('button', 'mBu_save', 'menuBar', '', 'Save');
    let mBu_load = domForge('button', 'mBu_load', 'menuBar', '', 'Load');
    let mBu_pdf = domForge('button', 'mBu_pdf', 'menuBar', '', '» PDF');
    let mBu_newTitle = domForge('button', 'mBu_newTitle', 'menuBar', '', 'New Title');
    let mIn_file = domForge('input', 'mIn_file', 'menuBar');

    mBu_menu.onclick = (event) => {
        mCont.getBoundingClientRect().x < 0 
            ?(menuSlideIn())
            :(menuSlideOut());
        event.target.blur();
    }
    mBu_save.onclick = () => dataSet.saveDoc();
    mBu_load.onmouseenter = () => (mBu_load.style.display = 'none', mIn_file.style.display = 'inline-block');
    mIn_file.onmouseout = () => (mBu_load.style.display = 'inline-block', mIn_file.style.display = 'none');
    mIn_file.onchange = (event) => loadDoc(event);

    mBu_newTitle.onclick = (event) => newEntry('ti', event.target);


    


    mField.append(mTi_file);
    mField.append(mBu_menu);
    mField.append(mBu_save);
    mField.append(mBu_load);
    mField.append(mIn_file);
    mField.append(mTi_document);
    mField.append(mBu_newTitle);

    let goGetBut = () => mBu_newTitle = document.getElementById('tB-1');
    setTimeout(goGetBut, 100);





    /** import Save from textfile */
    let loadDoc = (event, loadB = event.target, fileIn = loadB.files[0]) => {
        let fName = fileIn.name.toString();
        if (fName.substr(fName.length - 3) === 'LDo') {
            firstFileLoad = true;
            let fileReader = new FileReader();

            fileReader.onload = fileLoadedEvent => {
                let dataText = fileLoadedEvent.target.result;
                fillDoc(JSON.parse(dataText));
            }
            fileReader.readAsText(fileIn, "UTF-8");
        } else {
            window.alert('File not valid')
        }
    }
    let fillDoc = tempObject => {
        let fiList = document.getElementById('fiList');
        let fiTimer = '';
        fiList.childNodes.forEach(x => x.remove())
        fiList.innerHTML = '';
        mBu_newTitle.id = 'tB-1';
        
        let goFill = () => {
            let ti0 = document.getElementById('box_ti0');
            if (ti0 === null) {
                clearInterval(fiTimer);
                /** add titles */
                for (let i = 0; tempObject['ti'+i] !== undefined; i++) {
                    newEntry('ti', mBu_newTitle);
                    /** add rows */
                    let goAddRows = () => {
                        for (let n = 0; tempObject['ti'+i]['ro'+(n+1)] !== undefined; n++) {
                            let goRow = () => {
                                let rBut = document.getElementById('rB'+i+'_ro'+n);
                                rBut !== null 
                                    ?(rBut.click(),
                                    clearInterval(fiTimer)) 
                                    :''
                                ;
                            }
                            fiTimer = setInterval(goRow, 30);
                        };
                    };
                    setTimeout(goAddRows, 50);
                };
                window.firstFileLoad 
                    ?(window.firstFileLoad = false, 
                    fillDoc(tempObject)) 
                    :fillEntries(tempObject)
                ;
            }
        }
        fiTimer = setInterval(goFill, 100);
    }
    let fillEntries = (tO, fiTimer2 = '') => {
        /** tO >> dataSet */
        Object.setPrototypeOf(tO, DataSet.prototype);
        dataSet = tO;
        let lDiv = dataSet.lastEntry();
        /** fill entries as last entry exists */
        let goFillEntries = () => {
            let chkDiv = document.getElementById(lDiv);
            chkDiv !== null 
                ?(clearInterval(fiTimer2),
                dataSet.loadEntries())
                :''
            ;
        }
        fiTimer2 = setInterval(goFillEntries, 100)
    }
}