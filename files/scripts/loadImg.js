"use strict"
import domForge from '/scripts/domForge.js';

export default function loadImg(_img, _expt = '') { // _img - image DOM object | _expt - exception ['' => dialog, 'edit' => apply imageedit frame]
    
    let fiPic = _img.parentNode;
    let oLay = document.getElementById('overlay');

    /** eChk - attribute for img edit casehandler | eTimer - interval for edit */
    let eChk = 'false';
    let eTimer = {};

    /** iProps gets attributes of the actual image */    
    let iProps = {};
    iProps.h = () => _img.clientHeight;
    iProps.w = () => _img.clientWidth;
    iProps.l = () => Number(_img.style.left.substr(0, _img.style.left.indexOf('px')));
    iProps.t = () => Number(_img.style.top.substr(0, _img.style.top.indexOf('px')));
    iProps.nH = () => _img.naturalHeight;
    iProps.nW = () => _img.naturalWidth;
    iProps.tH = () => iProps.h() + iProps.t();
    iProps.tW = () => iProps.w() + iProps.l();
    iProps.ratio = () => iProps.nW() / iProps.nH();
    iProps.scale = () => iProps.h() / iProps.nH();
    iProps.pH = () => fiPic.clientHeight;
    iProps.pW = () => fiPic.clientWidth;


    /** 
     *  change (link) image DIALOG
     */
    let goDialog = (direct = '') => {
        let dChk = document.getElementById('imgDialog');
        if (dChk !== null && dChk !== undefined) {
            dChk.remove();
            oLay.style.display = 'none';
        } else {
            let dialog = domForge('div', 'imgDialog', 'imgDialog');
            let linkBut = domForge('button', 'linkBut', 'imgDialog', '', 'Enter a link');
            let uplBut = domForge('button', 'uplBut', 'imgDialog', '', 'Upload from Device');
            let linkInput = domForge('input', 'linkInput', 'imgDialog');
            let linkBack = domForge('button', 'linkBack', 'imgDialog', '', 'Back');
            let linkOK = domForge('button', 'linkOK', 'imgDialog', '', 'OK');
            let diaPaste = domForge('div', 'diaPaste', 'imgDialog', '', 'paste a link to your image');

            diaPaste.onmouseover = () => '';
            linkBut.onclick = () => enterLink();
            linkBack.onclick = () => enterDialog();
            linkOK.onclick = () => setLink();
            linkInput.onkeypress = (event) => kpLinkIn(event);
            _img.parentNode.append(dialog);
            

            let removeDialog = () => {
                dialog.remove();
                oLay.style.display = 'none';
            }
            let enterDialog = () => {
                oLay.style.display = 'block';
                linkBack.id === 'diaBack'
                    ?(linkBack.id = '',
                    removeDialog())
                    :enterLink();
                /*
                linkBack.id === 'diaBack' 
                    ?(linkBack.id = '', dialog.remove())
                    :(dialog.innerHTML = '',
                    linkBack.id = 'diaBack',
                    dialog.append(linkBack),
                    dialog.append(linkBut),
                    dialog.append(uplBut),
                    dialog.style.top = (iProps.h() - 105) + 'px');
                    */
            }
            let enterLink = () => {

                dialog.style.top = '80px'
                dialog.innerHTML = '';
                linkInput.value = '';
                linkBack.id = 'diaBack';
                dialog.append(diaPaste);
                dialog.append(linkInput);
                dialog.append(linkBack);
                dialog.append(linkOK);
                linkInput.focus();
            }

            /** apply a link to the DOM object */
            let setLink = () => {
                let leg = [iProps.nW(), iProps.nH()];
                _img.style.opacity = 0;
                _img.src = linkInput.value;
                removeDialog();
                sizeImg(leg);
            }
            let kpLinkIn = (event) => {
                event.keyCode === 13 ? setLink() : '';
            }

            
            direct === 'link' 
                ?enterLink()
                :enterDialog();
        };



    }


    /** resize on load */
    let sizeImg = (leg, interSize = '') => {
        let goSize = () => {
            leg[0] === iProps.nW() && leg[1] === iProps.nH()
                ?''
                :(_img.style.height = 250 + 'px',
                _img.style.width = (250 * iProps.ratio()) + 'px',
                _img.style.left = ((iProps.pW() - iProps.w()) / 2) + 'px',
                _img.style.top = ((iProps.pH() - iProps.h()) / 2) + 'px',
                _img.style.opacity = '1',
                clearInterval(interSize));
        }
        interSize = setInterval(goSize, 400);
    }
    let reSizeImg = (i = 0, interReSize = '') => {
        let goReSize = () => {
            iProps.h() === 250 || i>= 100
                ?clearInterval(interReSize) 
                :i++;
            
        }
        interReSize = setInterval(goReSize, 400);
    }

    /**
     *  img EDIT functions
     */
    let imgEditUI = () => {
        /* get dom elements */
        let iForm = domForge('div', 'iForm', 'imgEditButs');
        let upArrow = domForge('input', 'iUpArrow', 'imgEditButs', './images/arrow_u15x15.svg');
        let downArrow = domForge('input', 'iDownArrow', 'imgEditButs', './images/arrow_d15x15.svg');
        let leftArrow = domForge('input', 'iLeftArrow', 'imgEditButs', './images/arrow_l15x15.svg');
        let rightArrow = domForge('input', 'iRightArrow', 'imgEditButs', './images/arrow_r15x15.svg');
        let navi = domForge('img', 'iNavi', 'imgEditButs', './images/navi_45x15.svg');
        let spacer0 = domForge('img', 'iSpace0', 'imgEditButs', './images/spacer_15x15.svg');
        let spacer1 = domForge('img', 'iSpace1', 'imgEditButs', './images/spacer_15x15.svg');
        let spacer2 = domForge('img', 'iSpace2', 'imgEditButs', './images/spacer_15x15.svg');
        let spacer3 = domForge('img', 'iSpace3', 'imgEditButs', './images/spacer_15x15.svg');
        let spacer4 = domForge('img', 'iSpace4', 'imgEditButs', './images/spacer_15x15.svg');        
        let shrink = domForge('button', 'iShrink', 'imgEditButs');
        let grow = domForge('button', 'iGrow', 'imgEditButs');
        let upLink = domForge('input', 'iUppLink', 'imgEditButs', './images/icons/link_blue.svg');
        let upLoad = domForge('input', 'iUppLoad', 'imgEditButs', './images/folder_64x64.svg');
        
        /* apply listeners */
        [upArrow, downArrow, leftArrow, rightArrow, shrink, grow].forEach(x => {
            x.onmousedown = event => (ediTimer(event.target)),
            x.onmouseup = () => endEdiTimer(),
            x.onmouseout = () => endEdiTimer()
        });
        upLink.onclick = () => goDialog('link');
        fiPic.onmouseenter = () => (iForm.style.display = 'inline-block', fiPic.style.backgroundColor = window.color.shade);
        fiPic.onmouseleave = () => (iForm.style.display = 'none', fiPic.style.backgroundColor = window.color.backGround);

        /* append to document */
        let goEdit = () => {
            iForm.append(navi);
            iForm.append(upLink);
            iForm.append(spacer1);
            iForm.append(upArrow);
            iForm.append(spacer2);
            iForm.append(leftArrow);
            iForm.append(spacer0);
            iForm.append(rightArrow);
            iForm.append(spacer3);
            iForm.append(downArrow);
            iForm.append(spacer4);
            iForm.append(shrink);
            iForm.append(grow);
            fiPic.append(iForm);
            clearInterval(waitToEdit);
        }
        let waitToEdit = setInterval(goEdit, 100);
    };
    
    /** img edit timer */
    let ediTimer = (eTg) => {
        eChk = eTg;
        eTimer = setInterval(imgEditHandler, 40);
    }
    let endEdiTimer = () => {
        clearInterval(eTimer);
    }

    /** img edit casehandler */
    let imgEditHandler = (eT = '') => {
        eT = eChk.id,

        eT === 'iUpArrow' ?moveUp() :'';
        eT === 'iDownArrow' ?moveDown() :'';
        eT === 'iLeftArrow' ?moveLeft() :'';
        eT === 'iRightArrow' ?moveRight() :'';
        eT === 'iShrink' ?shrink() :'';
        eT === 'iGrow' ?grow(eChk) :'';
    };

    /** edit execution */
    let moveUp = () => iProps.t() > 0 ?_img.style.top = (iProps.t() - 2) + 'px' :'';
    let moveDown = () => iProps.tH() < iProps.pH() ?_img.style.top = (iProps.t() + 2) + 'px' :'';
    let moveLeft = () => iProps.l() > 0 ?_img.style.left = (iProps.l() - 2) + 'px' :'';
    let moveRight = () => iProps.tW() < iProps.pW() ?_img.style.left = (iProps.l() + 2) + 'px' :'';
    let shrink = () => {
        iProps.h() > 100 && iProps.w() > 100
            ?(_img.style.left = (iProps.l() + (iProps.scale() * 0.005 * iProps.nW())) + 'px',
            _img.style.top = (iProps.t() + (iProps.scale() * 0.005 * iProps.nH())) + 'px',
            _img.style.width = (iProps.scale() * 0.99 * iProps.nW()) + 'px',
            _img.style.height = (iProps.scale() * 0.99 * iProps.nH()) + 'px')
            :'';
        }    
    let grow = () => {
        iProps.h() < iProps.pH() && iProps.w() < iProps.pW()
            ?(_img.style.left = (iProps.l() - (iProps.scale() * 0.005 * iProps.nW())) + 'px',
            _img.style.top = (iProps.t() - (iProps.scale() * 0.005 * iProps.nH())) + 'px',
            _img.style.width = (iProps.scale() * 1.01 * iProps.nW()) + 'px',
            _img.style.height = (iProps.scale() * 1.01 * iProps.nH()) + 'px')
            :'';
        iProps.tH() > iProps.pH() ?_img.style.top = (iProps.pH() - iProps.h()) + 'px' :'';
        iProps.tW() > iProps.pW() ?_img.style.left = (iProps.pW() - iProps.w()) + 'px' :'';
        iProps.l() < 0 ? _img.style.left = '0px' :'';
        iProps.t() < 0 ? _img.style.top = '0px' :'';
    }
    let rePosition = () => {
        iProps.tH() > iProps.pH() ?_img.style.top = (iProps.pH() - iProps.h()) + 'px' :'';
        iProps.tW() > iProps.pW() ?_img.style.left = (iProps.pW() - iProps.w()) + 'px' :'';
        iProps.l() < 0 ? _img.style.left = '0px' :'';
        iProps.t() < 0 ? _img.style.top = '0px' :'';
        iProps.h() > iProps.pH() ?(_img.style.height = iProps.pH() + 'px', _img.style.width = iProps.pH() * iProps.ratio() + 'px') :'';
        iProps.w() > iProps.pW() ?(_img.style.width = iProps.pW() + 'px', _img.style.height = iProps.pW() / iProps.ratio() + 'px') :'';

    }
    


    /** function-entry handler */

    _expt === '' 
        ?goDialog()
        :_expt === 'edit'
            ?imgEditUI()
            :_expt === 'resize'
                ?reSizeImg()
                :_expt === 'setSize'
                    ?rePosition()
                    :'';
}