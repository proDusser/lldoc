"use strict"

/** DataSet is a class to get document content, store it
 * in a hierarchy and help in the save and load process.
 */

export default class DataSet {
    constructor() {
        this.img = {};
        this.pers = {};
        this.ti0 = {};
        this.ti0.ro0 = {};
    }
    idSplitter(_id) {return _id.split('_')};
    lastEntry(lTi = 0, lRo = 0) {
        while(this['ti' + (lTi+1)] !== undefined) lTi++;
        lTi = 'ti' + lTi;
        while(this[lTi]['ro' + (lRo+1)] !== undefined) lRo++;
        return lTi + '_ro' + lRo + '_va2';
    }
    catchDoc() {
        let idArr = [];
        let mTi = document.getElementById('fiMainTitle');
        let pic = document.getElementById('imgPic');
        let entries = window.arrMe(document.getElementsByClassName('entries'));
        let titles = window.arrMe(document.getElementsByClassName('title'));
        let persos = window.arrMe(document.getElementsByClassName('persIn'))

        this.color = window.color;
        this.textSize = window.textSize;
        this.mTitle = mTi.firstElementChild.innerHTML;
        this.img.src = pic.src;
        this.img.left = pic.style.left;
        this.img.top = pic.style.top;
        this.img.height = pic.style.height;
        this.img.width = pic.style.width;
        
        persos.forEach(x => this.pers[x.id.substr(x.id.indexOf('va'))] = x.value !== undefined ?x.value :'');
        titles.forEach(y => {
            idArr = this.idSplitter(y.id);
            this[idArr[0]] = {};
            this[idArr[0]][idArr[1]] = y.value !== undefined ?y.value :'';
        });
        entries.forEach(z => {
            idArr = this.idSplitter(z.id);
            this[idArr[0]][idArr[1]] = {};
        });
        entries.forEach(n => {
            idArr = this.idSplitter(n.id);
            this[idArr[0]][idArr[1]][idArr[2]] = n.value !== undefined ?n.value :'';
        });

        console.log(this)
        return JSON.stringify(this);
    }
    saveDoc(sText = this.catchDoc()) {
        let persIns = document.getElementsByClassName('persIn');
        let sBlob = new Blob([sText], {type:"text/plain"});
        let sUrl = window.URL.createObjectURL(sBlob);
        let fName = 'LLDocs_' + persIns[0].value + '_' + persIns[1].value + '.LDo';

        let dLink = document.createElement('a');
        dLink.download = fName;
        dLink.innerHTML = 'download file';
        dLink.href = sUrl;
        dLink.onclick = event => document.body.removeChild(event.target);
        dLink.style.display = 'none';
        document.body.appendChild(dLink);

        dLink.click()
    }
    loadEntries (tIde = '') {
        let mTitle = document.getElementById('mTitle');
        let _img = document.getElementById('imgPic');
        let persIns = arrMe(document.getElementsByClassName('persIn'));
        let titles = arrMe(document.getElementsByClassName('title'));
        let entries = arrMe(document.getElementsByClassName('entries'));
        mTitle.innerHTML = this.mTitle;
        _img.src = this.img.src;
        _img.style.left = this.img.left;
        _img.style.top = this.img.top;
        _img.style.height = this.img.height;
        _img.style.width = this.img.width;
        persIns.forEach(x => {
            tIde = x.id.split('_');
            x.value = this.pers[tIde[1]] !== undefined && this.pers[tIde[1]] !== undefined
                ?this.pers[tIde[1]]
                :'';
        });
        titles.forEach(y => {
            tIde = y.id.split('_');
            y.value = this[tIde[0]].va0;
        });
        entries.forEach(z => {
            tIde = z.id.split('_');
            tIde[2] === 'va2'
                ?z.innerHTML = this[tIde[0]][tIde[1]][tIde[2]]
                :z.value = this[tIde[0]][tIde[1]][tIde[2]]
            ;
        });
        window.scrollTo(0, 0);
    }

}


