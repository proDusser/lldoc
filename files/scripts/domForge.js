"use strict"

/** DOM element creation
 * eSrc and eHTML can be taken to define the case if eClass is not definite.
 * use 'exc_nameOfTheCase' as eSrc/eHTML to use the exception handler
 */

export default function domForge (eType, eID = '', eClass = '', eSrc =  '', eHTML = '', eStyle = {}) {
    
    let reForge = {};
    let hyperClass = '';
    let remS = parseFloat(getComputedStyle(document.documentElement).fontSize);
    let sRad = '0.25rem';
    let mRad = '0.375rem';
    let bRad = '0.5rem';


    let domCreate = () => {
        let reDom = document.createElement(eType);
        reDom.id = eID;
        reDom.class = eClass;
        reDom.className = eClass;
        reDom.src = eSrc;
        reDom.innerHTML = eHTML;
        return reDom;
    }



    /**-------------------*/
    /** img Edit elements */
    let imgEditButs = () => {
    
    reForge.style.width = '1rem',
    reForge.style.height = '1rem',
    reForge.style.margin = '0px',
    reForge.style.padding = '0px',
    reForge.style.float = 'left',
    reForge.style.pointerEvents = 'auto',
    

    eID === 'iForm' 
        ?(reForge.style.position = 'absolute',
        reForge.style.width = '4rem',
        reForge.style.height = '8.5rem',
        reForge.style.margin = '0rem',
        reForge.style.padding = '0.5rem',
        reForge.style.opacity = 1,
        reForge.style.zIndex = '2',
        reForge.style.float = 'left',
        reForge.style.display = 'none',
        reForge.style.top =  '-3rem', 
        reForge.style.left = 0,
        reForge.style.backgroundColor = window.color.shade,
        edgeTop(mRad))
        :'';
    eType === 'input' && eID.substr(0,4) !== 'iUpp' 
        ?reForge.type = 'image'
        :'';
    eType === 'input' && eID.substr(0,4) === 'iUpp'
        ?(reForge.type = 'image',
        reForge.style.width = '2rem',
        reForge.style.height = '2rem',
        reForge.style.marginTop = '0.25rem',
        reForge.style.marginRight = '0.5rem',
        reForge.style.marginLeft = '0.5rem',
        reForge.style.marginBottom  = '1rem')
        :'';
    eType === 'button'
        ?(reForge.style.width = '1.5rem',
        reForge.style.opacity = 0,
        reForge.style.zIndex = '3',
        reForge.style.marginTop = '0.5rem')
        :'';
    eType === 'img'
        ?(reForge.style.pointerEvents = 'none',
        eID === 'iNavi' 
            ?(reForge.style.zIndex = '2',
            reForge.style.width = '3rem',
            reForge.style.position = 'absolute',
            reForge.style.bottom = '0rem',
            reForge.style.left = '0.5rem')
            :(reForge.style.opacity = 0))
        :'';
    eID === 'iSpace0' 
        ?reForge.style.opacity = 1
        :'';
    
    }
    /*--------------------------*/
    /* img link and load dialog */
    let imgDialog = () => {
        let fiPic = document.getElementById('fiPic');
        let diaWidth = (fiPic.clientWidth - 4*remS)
        reForge.style.width = (diaWidth - remS) + 'px';
        reForge.style.height = '2rem';
        reForge.style.marginTop = '0.25rem';
        reForge.style.marginBottom = '0.25rem';
        reForge.style.padding = '0rem';
        reForge.style.border = 'transparent';
        reForge.style.color = window.color.text;
        reForge.style.backgroundColor = window.color.main;
        reForge.style.opacity = 1;
        reForge.style.zIndex = '4';
        reForge.style.textAlign = 'center'

        edgeSingle(sRad);

        eType === 'button' 
            ?(reForge.style.backgroundColor = window.color.shine,
            reForge.style.width = (diaWidth - 1.1*remS)/2 + 'px',
            eID === 'linkBack'
                ?edgeLeft(sRad)
                :(edgeRight(sRad),
                reForge.style.marginLeft = '0.1rem'))
            :'';
        eType === 'input' 
            ?(reForge.style.color = 'black',
            reForge.style.backgroundColor = 'white',
            reForge.style.textAlign = 'left')
            :'';
        eID === 'imgDialog' 
            ?(reForge.style.height = '7rem',
            reForge.style.marginLeft = '4rem',
            reForge.style.padding = '0.5rem',
            reForge.style.paddingTop = '0rem',
            reForge.style.width = diaWidth + 'px',
            reForge.style.position = 'absolute',
            edgeSingle(mRad))
            :''; 
        eID === 'diaPaste'
            ?(edgeTop(sRad),
            reForge.style.backgroundColor = window.color.main, 
            reForge.style.marginBottom = '-0.5rem',
            reForge.style.fontWeight = 'bold')
            :'';   
    }

    /*--------------------*/
    /* new Perso elements */
    let forgePersoButs = () => {
        reForge.style.padding = 0;
        reForge.style.margin = 0;
        eType === 'div'
            ?(reForge.style.pointerEvents = 'auto',
            reForge.style.width = '100%',
            reForge.style.height = '2.5rem',
            reForge.style.display = 'none',
            reForge.style.paddingTop = '0.5rem',
            reForge.style.backgroundColor = 'inherit',
            edgeSingle(mRad))
            :'';
        eType === 'button'
            ?(reForge.style.height = '2rem',
            reForge.style.border = 'transparent',
            reForge.style.color = window.color.text,
            reForge.style.backgroundColor = window.color.main,
            reForge.pointerEvents = 'auto',
            reForge.style.width = '49.8%',

            eID === 'addPersoBut' ?(reForge.style.marginLeft = '1px', edgeRight(sRad)) :'',
            eID === 'delPersoBut' ?edgeLeft(sRad) :'')
            :'';

        }

    /*---------------------------------*/
    /* new Title elements and docSpacer*/
    let forgeNewTitle = () => {
        reForge.style.width = '100%';
        eClass === 'roButs' 
            ?(reForge.style.pointerEvents = 'auto',
            reForge.style.width = (21.125 * remS) + 'px',
            reForge.style.backgroundColor = window.color.main,
            edgeSingle(sRad))
            :'';
        eID.substr(0, 6) === 'box_ti'
            ?(reForge.style.borderRadius = mRad,
            reForge.style.paddingLeft = '2rem',
            reForge.style.paddingRight = '2rem')
            :'';
        eID.substr(0, 6) === 'box_rB'
            ?(reForge.style.pointerEvents = 'auto',
            reForge.style.width = '100%',
            reForge.style.float = 'right',
            reForge.style.display = 'none',
            reForge.style.padding = '0.5rem',
            reForge.style.paddingTop = '0rem')
            :'';
        eID.substr(0, 2) === 'ti' && eType === 'div'
            ?(reForge.style.width = '100%',
            reForge.style.paddingBottom = '1rem',
            reForge.style.paddingTop = '1rem',
            reForge.style.pointerEvents = 'auto',
            reForge.style.fontSize = (window.textSize.title/12) + 'rem')
            :'';
        eType === 'button' 
            ?edgeSingle(sRad)
            :''
    }
    let forgeDocSpacer = () => {
        reForge.style.pointerEvents = 'auto';

        eID === 'docSpacer'
            ?(reForge.style.width = '98%',
            //reForge.style.height = '24px',
            reForge.style.marginLeft = '1%',
            reForge.style.marginRight = '1%',
            reForge.style.display = 'flex',
            reForge.style.borderTopStyle = 'none',
            reForge.style.borderBottomStyle = 'none')
            :'';
        eID.substr(0, 8) === 'spaceBut'
            ?(reForge.type = 'image',
            reForge.style.width = '3%',
            reForge.style.height = '100%',
            reForge.style.minHeight = '18px',
            reForge.style.backgroundColor = window.color.main,
            reForge.style.border = 'transparent',
            reForge.style.opacity = 0.8,
            reForge.style.float = 'left',
            reForge.style.display = 'none',
            edgeSingle(sRad),
            eID === 'spaceBut0' 
                ?(reForge.style.marginLeft = '46.5%',
                reForge.style.marginRight = '0.5%') 
                :(reForge.style.marginRight = '46.5%',
                reForge.style.marginLeft = '0.5%'))
            :'';

        
        }


    /*------------------*/
    /* new Row elements */
    let forgeNewRow = () => {
        eClass === 'row'
            ?reForge.style.width = '100%'
            :'';
        eClass === 'col-md-2' || eClass === 'col-md-8'
            ?(reForge.style.float = 'left',
            reForge.style.pointerEvents = 'auto'):
            '';
        eClass === 'entries'
            ?(reForge.style.float = 'left',
            reForge.style.pointerEvents = 'auto',
            reForge.style.width = '100%',
            reForge.style.marginTop = '0.4rem',
            reForge.style.marginBottom = '0.4rem',
            reForge.style.paddingBottom = '1px',
            reForge.style.paddingTop = '1px',
            reForge.style.minHeight = '1.625rem',
            reForge.style.fontSize = (window.textSize.entries/12) + 'rem')
            :'';
    }
    let forgeFocusButs = () => {
        eType === 'div'
            ?(reForge.style.pointerEvents = 'auto',
            reForge.style.width = '100%',
            reForge.style.display = 'block',
            reForge.style.padding = '0.5rem')
            :'';
        eType = 'button'
            ?(reForge.pointerEvents = 'auto',
            reForge.style.marginLeft = '1px',
            eHTML === 'Down' ?(reForge.style.backgroundColor = window.color.main, edgeLeft(sRad)) :'',
            eHTML === 'Up' ?(reForge.style.backgroundColor = window.color.main, edgeCenter(sRad)) :'',
            eHTML === 'Delete' ?(reForge.style.backgroundColor = window.color.main, edgeRight(sRad)) :'')
            :'';
    }

    /*---------------------*/
    /* hover Menu elements */
    let forgeHoverMenu = () => {
        reForge.style.margin = '0rem';
        reForge.style.padding = '0rem';
        reForge.style.width = '1rem';
        reForge.style.height = '1rem';
        reForge.style.float = 'right';
        reForge.style.zIndex = '2';
        reForge.style.pointerEvents = 'auto';
        reForge.style.color = window.color.main;

        eID.substr(0, 3) === 'arr'
            ?(reForge.type = 'image',
            eSrc.indexOf('_u_') !== -1
                ?reForge.style.borderTopRightRadius = sRad
                :reForge.style.borderBottomRightRadius = sRad)
            :'';
        eID.substr(0, 3) === 'fra'
            ?(reForge.style.height = '3rem',
            reForge.style.position = 'absolute',
            reForge.style.display = 'none',
            eStyle === 'ti'
                ?(reForge.style.right = '2.6rem',
                reForge.style.width = '12rem')
                :'',
            eStyle === 'pe'
                ?(reForge.style.height = '3.2rem',
                reForge.style.right = '0rem',
                reForge.style.top = '-3rem',
                reForge.style.width = '9.2rem',
                reForge.style.paddingLeft = '0.0rem',
                reForge.style.paddingRight = '0.5rem',
                reForge.style.backgroundColor = window.color.shade,
                edgeTop(mRad))
                :'')
            :'';
        eID.substr(0, 3) === 'tit'
            ?(eID === 'title0H' 
                ?reForge.style.width = '4.5rem'
                :reForge.style.width = '3rem',
            reForge.style.height = '1rem',
            reForge.style.fontSize = '0.66rem',
            reForge.style.marginLeft = '0.5rem')
            :'';
        eID.substr(0, 4) === 'imgB'
            ?(reForge.type = 'image',
            reForge.style.width = '2rem',
            reForge.style.height = '2rem',
            edgeSingle(sRad),
            eID === 'imgBHover1' 
                ?reForge.style.marginLeft = '0.5rem'
                :'')
            :'';
        eID.substr(0, 4) === 'imgS'
            ?(reForge.type = 'image',
            reForge.style.height = '2rem',
            reForge.style.marginLeft = '0.2rem',
            edgeSingle(sRad))
            :'';
        eID.substr(0, 3) === 'tSi'
            ?(reForge.type = 'number',
            reForge.style.width = '2rem',
            reForge.style.height = '2rem',
            reForge.style.paddingBottom = '0.1rem',
            reForge.style.marginLeft = '0.5rem',
            reForge.style.fontSize = '1.375rem',
            reForge.style.textAlign = 'center',
            reForge.style.borderStyle = 'solid',
            reForge.style.borderColor = window.color.main,
            reForge.style.color = window.color.shine,
            edgeLeft(sRad))
            :'';
        eID.substr(0, 3) === 'div'
            ?reForge.style.height = '2rem'
            :'';
    }

    /*---------------------*/
    /* Menu Bar elements */
    let forgeMenuBar = () => {
        let eIDs = eID.split('_');
        reForge.style.float = 'left';
        reForge.style.color = window.color.text;
        reForge.style.display = 'inline-block';
        reForge.style.height = '2.5rem';
        reForge.style.width = '7.5rem';
        reForge.style.marginBottom = '0.125rem';
        reForge.style.marginTop = '0.125rem';
        reForge.style.marginLeft = '0rem';
        reForge.style.border = 'transparent';
        reForge.style.outline = 'none';
        edgeRight(sRad);
        eIDs[0] === 'mBu'
            ?(eIDs[1] === 'menu'
                ?(reForge.style.width = '2.8rem',
                reForge.style.backgroundColor = window.color.main,
                reForge.style.marginBottom = '1.625rem',
                reForge.style.marginTop = '0rem',
                reForge.style.paddingBottom = '0.25rem',
                reForge.style.paddingRight = '0.25rem',
                reForge.style.fontWeight = 'bold')
                :(reForge.style.backgroundColor = window.color.shine),
            eIDs[1] === 'pdf'
                ?(reForge.style.display = 'none')
                :'',
            eIDs[1] === 'newTitle'
                ?reForge.id = 'tB-1'
                :'')
            :'';
        eIDs[0] === 'mTi'
            ?(reForge.style.fontWeight = 'bold',
            reForge.style.width = '5rem',
            reForge.style.paddingLeft = '0.5rem',
            reForge.style.paddingTop = '1rem',
            eIDs[1] === 'file'
                ?reForge.style.marginTop = '1.5rem'
                :'')
            :'';
        eIDs[0] === 'mIn'
            ?(reForge.type = 'file',
            reForge.style.display = 'none',
            reForge.style.paddingTop = '0.5rem',
            reForge.style.paddingBottom = '0.5rem',
            reForge.style.paddingLeft = '1rem',
            reForge.style.backgroundColor = window.color.shine)
            :'';
    }




    /** short edits */

    let edgeSingle = (rad) => {
        reForge.style.borderTopLeftRadius = rad;
        reForge.style.borderTopRightRadius = rad;
        reForge.style.borderBottomLeftRadius = rad;
        reForge.style.borderBottomRightRadius = rad;
    }
    let edgeLeft = (rad) => {
        reForge.style.borderTopLeftRadius = rad;
        reForge.style.borderTopRightRadius = '0rem';
        reForge.style.borderBottomLeftRadius = rad;
        reForge.style.borderBottomRightRadius = '0rem';
    }    
    let edgeRight = (rad) => {
        reForge.style.borderTopLeftRadius = '0rem';
        reForge.style.borderTopRightRadius = rad;
        reForge.style.borderBottomLeftRadius = '0rem';
        reForge.style.borderBottomRightRadius = rad;
    }
    let edgeTop = (rad) => {
        reForge.style.borderTopLeftRadius = rad;
        reForge.style.borderTopRightRadius = rad;
        reForge.style.borderBottomLeftRadius = '0rem';
        reForge.style.borderBottomRightRadius = '0rem';
    }    
    let edgeBottom = (rad) => {
        reForge.style.borderTopLeftRadius = '0rem';
        reForge.style.borderTopRightRadius = '0rem';
        reForge.style.borderBottomLeftRadius = rad;
        reForge.style.borderBottomRightRadius = rad;
    }  
    let edgeCenter = () => {
        reForge.style.borderTopLeftRadius = '0rem';
        reForge.style.borderTopRightRadius = '0rem';
        reForge.style.borderBottomLeftRadius = '0rem';
        reForge.style.borderBottomRightRadius = '0rem';
    }
    







    /* handle exceptions with hyperClass */
    let exceptionHandler = (infoSlot, element) => {
        infoSlot === 'eSrc' ?eSrc = '' :'';
        infoSlot === 'eHTML' ?eHTML = '' :'';
        hyperClass = element;
    }

    /* check for excepptions */
    eSrc.split('_')[0] === 'exc'
        ?exceptionHandler('eSrc', eSrc.split('_')[1]) 
        :eHTML.split('_')[0] === 'exc' 
            ?exceptionHandler('eHTML', eHTML.split('_')[1]) 
            :hyperClass = '';
    
    reForge = domCreate();
    /* handle classes */
    reForge.class === 'imgEditButs' ?imgEditButs() :'';
    reForge.class === 'imgDialog' ?imgDialog() :'';
    reForge.class === 'hMenu' ?forgeHoverMenu() :'';
    reForge.class === 'persoButs' ?forgePersoButs() :'';
    reForge.class === 'docSpacer' ?forgeDocSpacer() :'';
    reForge.class === 'menuBar' ?forgeMenuBar() :'';
    hyperClass === 'newTitle' ?forgeNewTitle() :'';
    hyperClass === 'newRow' ?forgeNewRow() :'';
    hyperClass === 'focusButs' ?forgeFocusButs() :'';

    return reForge;
}