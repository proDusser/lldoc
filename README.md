# LLDocs - CV build and edit page


## goals

 * content: Title, Personal infos, Picture, Short description, multiple Titles with mutliple rows
 * save document in local storage and lets the user download a .txt (JSON) to save locally
 * import .txt saves
 * layout-tool and pdf-export
 * adding links and emails is possible


## existing functions
 
 * all inputs except short description
 * input discovers links
 * add titles, add rows
 * edit row buttons (up, down, delete)
 * link an image
 * save document in object for json string
 * basic css

## planned functions

 * implement imgur api to upload and link local images
 * repair textsize option
 * import from json string
 * export .txt file
 * add short description
 * style (rebuild) img & link input