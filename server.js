"use strict";

const express = require('express');
const app = new express();
const port = 3000;
const fs = require('fs');

app.use(express.static('files'));

app.get('/html/*', (req, res) => {
    let path = app.get(express.static) + req.params[0];
    res.sendFile(path);
});
app.get('/scripts/*', (req, res) => {
    res.type('text/javascript');
    let path = app.get(express.static) + req.params[0];
    res.sendFile(path);
});
app.get('/css/*', (req, res) => {
    res.type('text/css');
    let path = app.get(express.static) + req.params[0];
    res.sendFile(path);
});
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
